'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema
    ;

/**
 *  Organization schema
 */
var OrganizationSchema = new Schema({
  name : String
});

module.exports = mongoose.model('Organization', OrganizationSchema);