'use strict';

var sockets = require('socket.io'),
    _ = require('underscore')
    ;

// @function connects socket listeners to individual connections
var prepareSocketConnections = function (socket, io) {
    // TODO
};

// @function connects listeners to the global websocket object
var prepareGlobalSocketConnections = function (io) {

    // TODO

    io.sockets.on('connection', function (socket) {

        socket.set('id', _.uniqueId('socket::'));
        prepareSocketConnections(socket, io);
    });
};

/**
 *  Application (web) sockets
 */
module.exports = function (server) {

    var io = sockets.listen(server);
    prepareGlobalSocketConnections(io);
};