(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
require('./ng');
},{"./ng":11}],2:[function(require,module,exports){
'use strict';

angular.module('app')
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

      // unmatched URLs go to /
      $urlRouterProvider.otherwise('/');

      $stateProvider
        .state('index', {
          url : '/',
          template : '<h1>hey</h1>'
        })
        ;
    }
  ]);
},{}],3:[function(require,module,exports){
'use strict';

angular.module('app')
  .controller('LoginCtrl', [
    '$scope',
    'Auth',
    '$location',
    function ($scope, Auth, $location) {
      $scope.user = {};
      $scope.errors = {};

      $scope.login = function(form) {
        $scope.submitted = true;

        if(form.$valid) {
          Auth.login({
            email: $scope.user.email,
            password: $scope.user.password
          })
          .then( function() {
            // Logged in, redirect to home
            $location.path('/');
          })
          .catch( function(err) {
            err = err.data;
            $scope.errors.other = err.message;
          });
        }
      };
    }
  ]);
},{}],4:[function(require,module,exports){
'use strict';

angular.module('app')
  .controller('MainCtrl', [
    function () {

    }
  ]);

},{}],5:[function(require,module,exports){
'use strict';

angular.module('app')
  .controller('NavbarCtrl', [
    '$scope',
    '$location',
    'Auth',
    function ($scope, $location, Auth) {
      $scope.menu = [{
        'title': 'Home',
        'link': '/'
      }, {
        'title': 'Settings',
        'link': '/settings'
      }];

      $scope.logout = function() {
        Auth.logout()
        .then(function() {
          $location.path('/login');
        });
      };

      $scope.isActive = function(route) {
        return route === $location.path();
      };
    }
  ]);

},{}],6:[function(require,module,exports){
'use strict';

angular.module('app')
  .controller('SettingsCtrl', [
    '$scope',
    'User',
    'Auth',
    function ($scope, User, Auth) {
      $scope.errors = {};

      $scope.changePassword = function(form) {
        $scope.submitted = true;

        if(form.$valid) {
          Auth.changePassword( $scope.user.oldPassword, $scope.user.newPassword )
            .then( function() {
              $scope.message = 'Password successfully changed.';
            })
            .catch( function() {
              form.password.$setValidity('mongoose', false);
              $scope.errors.other = 'Incorrect password';
            });
        }
      };
    }
  ]);

},{}],7:[function(require,module,exports){
'use strict';

angular.module('app')
  .controller('SignupCtrl', [
    '$scope',
    'Auth',
    '$location',
    function ($scope, Auth, $location) {
      $scope.user = {};
      $scope.errors = {};

      $scope.register = function(form) {
        $scope.submitted = true;

        if(form.$valid) {
          Auth.createUser({
            name: $scope.user.name,
            email: $scope.user.email,
            password: $scope.user.password
          })
          .then( function() {
            // Account created, redirect to home
            $location.path('/');
          })
          .catch( function(err) {
            err = err.data;
            $scope.errors = {};

            // Update validity of form fields that match the mongoose errors
            angular.forEach(err.errors, function(error, field) {
              form[field].$setValidity('mongoose', false);
              $scope.errors[field] = error.message;
            });
          });
        }
      };
    }
  ]);
},{}],8:[function(require,module,exports){
'use strict';

angular.module('app')

  /**
   * Removes server error when user updates input
   */
  .directive('mongooseError', [
    function () {
      return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
          element.on('keydown', function() {
            return ngModel.$setValidity('mongoose', true);
          });
        }
      };
    }
  ]);
},{}],9:[function(require,module,exports){
angular.module('entities', [
]);
},{}],10:[function(require,module,exports){
angular.module('app', [
    'ngResource',
    'ngCookies',
    'ngSanitize',
    'ui.router',
    'entities'
  ]);

},{}],11:[function(require,module,exports){
// load angular js resources

// @modules
require('./modules/app');
require('./modules/entities');

// @configurations
require('./configs/routing');

// @controllers
require('./controllers/login');
require('./controllers/main');
require('./controllers/navbar');
require('./controllers/settings');
require('./controllers/signup');

// @directives
require('./directives/mongooseError');

// @services
require('./services/auth');
require('./services/session');
require('./services/user');
},{"./configs/routing":2,"./controllers/login":3,"./controllers/main":4,"./controllers/navbar":5,"./controllers/settings":6,"./controllers/signup":7,"./directives/mongooseError":8,"./modules/entities":9,"./modules/app":10,"./services/auth":12,"./services/session":13,"./services/user":14}],12:[function(require,module,exports){
'use strict';

angular.module('app')
  .factory('Auth', [
    '$location',
    '$rootScope',
    'Session',
    'User',
    '$cookieStore',
    function Auth($location, $rootScope, Session, User, $cookieStore) {

      // Get currentUser from cookie
      $rootScope.currentUser = $cookieStore.get('user') || null;
      $cookieStore.remove('user');

      return {

        /**
         * Authenticate user
         *
         * @param  {Object}   user     - login info
         * @param  {Function} callback - optional
         * @return {Promise}
         */
        login: function(user, callback) {
          var cb = callback || angular.noop;

          return Session.save({
            email: user.email,
            password: user.password
          }, function(user) {
            $rootScope.currentUser = user;
            return cb();
          }, function(err) {
            return cb(err);
          }).$promise;
        },

        /**
         * Unauthenticate user
         *
         * @param  {Function} callback - optional
         * @return {Promise}
         */
        logout: function(callback) {
          var cb = callback || angular.noop;

          return Session.delete(function() {
              $rootScope.currentUser = null;
              return cb();
            },
            function(err) {
              return cb(err);
            }).$promise;
        },

        /**
         * Create a new user
         *
         * @param  {Object}   user     - user info
         * @param  {Function} callback - optional
         * @return {Promise}
         */
        createUser: function(user, callback) {
          var cb = callback || angular.noop;

          return User.save(user,
            function(user) {
              $rootScope.currentUser = user;
              return cb(user);
            },
            function(err) {
              return cb(err);
            }).$promise;
        },

        /**
         * Change password
         *
         * @param  {String}   oldPassword
         * @param  {String}   newPassword
         * @param  {Function} callback    - optional
         * @return {Promise}
         */
        changePassword: function(oldPassword, newPassword, callback) {
          var cb = callback || angular.noop;

          return User.update({
            oldPassword: oldPassword,
            newPassword: newPassword
          }, function(user) {
            return cb(user);
          }, function(err) {
            return cb(err);
          }).$promise;
        },

        /**
         * Gets all available info on authenticated user
         *
         * @return {Object} user
         */
        currentUser: function() {
          return User.get();
        },

        /**
         * Simple check to see if a user is logged in
         *
         * @return {Boolean}
         */
        isLoggedIn: function() {
          var user = $rootScope.currentUser;
          return !!user;
        },
      };
    }
  ]);
},{}],13:[function(require,module,exports){
'use strict';

angular.module('app')
  .factory('Session', [
    '$resource',
    function ($resource) {
      return $resource('/api/session/');
    }
  ]);

},{}],14:[function(require,module,exports){
'use strict';

angular.module('entities')
  .factory('User', [
    '$resource',
    function ($resource) {
      return $resource('/api/users/:id', {
        id: '@id'
      }, { //parameters default
        update: {
          method: 'PUT',
          params: {}
        },
        get: {
          method: 'GET',
          params: {
            id:'me'
          }
        }
      });
    }
  ]);

},{}]},{},[1])