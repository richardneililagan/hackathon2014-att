'use strict';

angular.module('app')
  .factory('Session', [
    '$resource',
    function ($resource) {
      return $resource('/api/session/');
    }
  ]);
