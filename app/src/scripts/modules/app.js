angular.module('app', [
    'ngResource',
    'ngCookies',
    'ngSanitize',
    'ui.router',
    'entities'
  ]);
