// load angular js resources

// @modules
require('./modules/app');
require('./modules/entities');

// @configurations
require('./configs/routing');

// @controllers
require('./controllers/login');
require('./controllers/main');
require('./controllers/navbar');
require('./controllers/settings');
require('./controllers/signup');

// @directives
require('./directives/mongooseError');

// @services
require('./services/auth');
require('./services/session');
require('./services/user');