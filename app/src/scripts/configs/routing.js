'use strict';

angular.module('app')
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

      // unmatched URLs go to /
      $urlRouterProvider.otherwise('/');

      $stateProvider
        .state('index', {
          url : '/',
          template : '<h1>hey</h1>'
        })
        ;
    }
  ]);