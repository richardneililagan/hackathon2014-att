'use strict';

var mongoose = require('mongoose'),
    _q = require('q')
    ;

// models
var User = mongoose.model('User'),
    Organization = mongoose.model('Organization')
    ;

/**
 * Populate database with sample application data
 */
_q.when(function () {
    var task = _q.defer();

    // clear old organizations, then add an organization
    Organization.find({}).remove(function () {
      var org = new Organization({
        name : 'Test Organization'
      });
      org.save(function (err) {
        if (err) {
          task.reject('Error while updating organizations.');
        }
        else {
          task.resolve(org);
        }
      });
    });

    return task;
  })
  .then(function (org) {

    var task = _q.defer();

    // clear old users, then add a default user
    User.find({}).remove(function () {

      var user = new User({
        provider : 'local',
        name : 'Test User',
        email : 'test@test.com',
        password : 'test',
        organizations : [org]
      });

      user.save(function (err) {
        if (err) {
          task.reject('Error while updating users.');
        }
        else {
          task.resolve(user);
        }
      });
    });

    return task;
  })
  .done(function () {
    console.log('Finished populating dummy data.');
  });